<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

$thumb = wp_get_attachment_image_src( get_post_thumbnail_id( wc_get_page_id( 'shop' ) ), 'full' );
if(empty($thumb)){
	$thumb['0'] = get_template_directory_uri().'/images/shop-banner.jpg';
}?>
<header class="slider__section inner__section inline__block">
	<div class="container tbl">
		<div class="tbl-cell">
			<div class="slider__content">
				<h2>Shop</h2>
			</div>
		</div>
	</div>
	<div class="bgoverlay" style="background-image: url('<?php echo $thumb['0'];?>');"></div>
	<div class="gredientoverlay" style="background: rgba(0, 0, 0, 0.396003);"></div>
</header>

<?php 
/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );
?>
<section class="shop__sect tacenter  inline__block paddingtop">
	<div class="container">
		<div class="container__inner">
			<div class="shop__top common__sect inline__block">
				<h2>Filters</h2>
				<div class="woo__tools">					
					<?php
					$terms = get_terms( array(
						'taxonomy' => 'product_cat',
						'hide_empty' => true,
						'orderby'  => 'name',
						'order' => 'ASC'
					));
					if(!empty($terms)) {?>
						<div class="woocommerce-ordering">
							<select name="productcat" class="productcat" aria-label="Shop order">
								<option value="">Choose Category</option>
								<?php 
								foreach ($terms as $term) { 
									$parentName = sanitize_title($term->name);
									if($parentName != "uncategorized" ){?>
										<option value="<?php echo $parentName;?>"><?php echo $parentName;?></option>
									<?php }
								}?>
							</select>
						</div>
					<?php } ?>
					<div class="woocommerce-ordering">
						<select name="orderby" class="orderby" aria-label="Shop order">
							<option value="menu_order" selected="selected">Default sorting</option>
							<option value="popularity">Sort by popularity</option>
							<option value="rating">Sort by average rating</option>
							<option value="date">Sort by latest</option>
							<option value="price">Sort by price: low to high</option>
							<option value="price-desc">Sort by price: high to low</option>
						</select>
					</div>
				</div>
			</div>
			<div class="shop__listing inline__block paddingtop">
				
					<?php
					if ( woocommerce_product_loop() ) {

						woocommerce_product_loop_start();

						if ( wc_get_loop_prop( 'total' ) ) {
							while ( have_posts() ) {
								the_post();

								/**
								 * Hook: woocommerce_shop_loop.
								 */
								do_action( 'woocommerce_shop_loop' );
								wc_get_template_part( 'content', 'product' );
							}
						}

						woocommerce_product_loop_end();?>

						<div class="shop_div inline__block tacenter shop_more">
						  	<a href="javascript:;" class="cmnbtn shopshowmore">Show more <span class="gg-shape-triangle"></span></a>
						  	<div class="shop-loadmore-img" style="display: none;">
								<img class="" src="<?php echo get_template_directory_uri(); ?>/images/loadmore.svg">
							</div>
							<p class="error" style="display: none;">No product found</p>
						</div>

						<?php
						//do_action( 'woocommerce_after_shop_loop' );
					} else {
						/**
						 * Hook: woocommerce_no_products_found.
						 *
						 * @hooked wc_no_products_found - 10
						 */
						do_action( 'woocommerce_no_products_found' );
					}

					/**
					 * Hook: woocommerce_after_main_content.
					 *
					 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
					 */
					do_action( 'woocommerce_after_main_content' );?>
			</div>
		</div>
	</div>
</section>
<?php
get_footer( 'shop' );
?>
<!-- Shop filter Ajex -->
<script type="text/javascript">
    var gtrack_page = 5;
    var loading  = false;
    var gppp = 3;
    var ajax_running = 0;

    function load_contents(orderby,cat,obj) {
		if(loading == false){
		  loading = true;
		  if(obj == 'true'){
		    var dataString  = {
		      'productcat': cat,
		      'obj'	  : obj,
		      'sortingval' : orderby,
		      'action': 'loadmoreproducts'
		    };
		  }
		  else {
		    var dataString  = {
		      'pageNumber': gtrack_page,
		      'ppp': gppp,
		      'productcat': cat,
		      'sortingval': orderby,
		      'action': 'loadmoreproducts'
		    };
		  }

		  ajax_running++;
		  if(ajax_running == 1){
		    var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
		    jQuery('.shop-loadmore-img').show();
		    jQuery.ajax({
				type: "POST",
				url: ajaxurl,
				data: dataString,
				success: function(data){
			        gtrack_page++;
			        loading = false;
			        if(jQuery.trim(data) != '') {
						jQuery('.shop_more .error').hide();						
						if(obj == 'true'){
							gtrack_page = 5;
						  	jQuery(".shop__listing ul").html(data);
						  	jQuery('.shopshowmore').show();
						} else {
						  	jQuery(".shop__listing ul").append(data);
						  	jQuery('.shopshowmore').show();
						}					
			        } else {
			        	if(obj == 'true'){
						  	jQuery(".shop__listing ul").html(data);						  	
						} else {
						  	jQuery(".shop__listing ul").append(data);
						}
						jQuery('.shopshowmore').hide();
			        	jQuery('.shop_more .error').show();
			        }
			        jQuery('.shop-loadmore-img').hide();
			        ajax_running = 0;
			  	}
		    });
		  }
		}
		return false;
	}
	/*Load More Button*/
    jQuery(document).on("click",".shopshowmore",function() {
        loading = false;
        var obj = '';
        var orderby = jQuery('.woocommerce-ordering .orderby option:selected').val();
        var cat = jQuery('.woocommerce-ordering .productcat option:selected').val();
        load_contents(orderby,cat,obj);
    });
    /*Orderby*/
    jQuery(document).on("change",".woocommerce-ordering .orderby",function() {
    	gtrack_page = '';
    	var gtrack_page = 5;    	
		loading = false;
		var obj = 'true';
	    var orderby = jQuery('.woocommerce-ordering .orderby option:selected').val();
        var cat = jQuery('.woocommerce-ordering .productcat option:selected').val();
		load_contents(orderby,cat,obj);
	});
	/*Category*/
	jQuery(document).on("change",".woocommerce-ordering .productcat",function() {
		gtrack_page = '';
		var gtrack_page = 5;
		loading = false;
		var obj = 'true';
	    var orderby = jQuery('.woocommerce-ordering .orderby option:selected').val();
        var cat = jQuery('.woocommerce-ordering .productcat option:selected').val();
		load_contents(orderby,cat,obj);
	});

</script>