<?php
/**
 * Template Name: News Template
 */
get_header(); ?>

<?php get_template_part('template-parts/page','banner' );?>

<section class="blog__lists inline__block">
	<div class="blog__filters inline__block">
		<div class="container">
			<ul class="button-group-home filters-button-group">
				<li class="button is-checked"><a href="javascript:;" data-value="All" class="active">Show all</a></li>
				<?php 
		            $terms = get_terms( 'category', array (
		                'order'   => 'ASC',
		                'hide_empty' => false,
		            ));
		            if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
		                foreach ( $terms as $term ) { 
		                	if($term->name != "Uncategorized"){?>
								<li class="button"><a href="javascript:;" data-value="<?php echo $term->slug; ?>"><?php echo $term->name; ?></a></li>
							<?php }
						}
            		} ?>
			</ul>
		</div>
	</div>
	<?php
	wp_reset_postdata();
	$postsPerPage = 9;
	$args = array(
	    'post_type' => 'post',
	    'orderby' => 'date',
	    'order' => 'DESC',
	    'post_status' => 'publish',
	    'posts_per_page' => $postsPerPage
	);
	$query = new WP_Query($args);
	if($query ->have_posts() ):
	?>
	<div class="blog__listings inline__block paddingtop">
		<div class="container">
			<div class="grid" id="grid">
				<?php 
				while ($query->have_posts()) : $query->the_post();
				$term_obj_list = get_the_terms( get_the_ID(), 'category' );
				$terms_string  = join(' ', wp_list_pluck($term_obj_list, 'name'));
				$postImage  = get_the_post_thumbnail_url();
				if(empty($postImage)){
					$postImage = get_template_directory_uri().'/images/placeholder.png';
				}?> 
					<div class="grid-item overview-block-product">
						<a href="<?php echo get_permalink(); ?>">
							<div class="shopbg bgproperty" style="background-image: url('<?php echo $postImage;?>');"></div>
							<div class="state_details inline__block">
								<div class="category"><?php echo $terms_string;?></div>
								<h3><?php echo get_the_title(); ?></h3>
								<p><?php echo get_the_excerpt();?></p>
								<a href="<?php echo get_permalink(); ?>" class="cmnbtn">Read more <span class="gg-shape-triangle"></span></a>
							</div>
						</a>
					</div>
				<?php endwhile;?>			
			</div>
			<div class="shop_div inline__block tacenter show-more-data">
				<a href="javascript:;" class="cmnbtn more_posts">show more news</span></a>
				<p class="no-post" style="display: none;">No more post found.</p>
				<div class="load-more-img" style="display: none;">
					<img class="" src="<?php echo get_template_directory_uri()?>/images/loadmore.svg">
				</div>
			</div>
		</div>
	</div>
	<?php endif;?>
</section>

<?php get_footer(); ?>

<script type="text/javascript">
	jQuery(document).ready(function($){
		var allOptions = jQuery("ul.filters-button-group").children('a');
		jQuery(".blog__lists ul.filters-button-group li").on("click", "a", function() {			
			jQuery(".blog__lists ul.filters-button-group li a").removeClass('active');
			jQuery(".blog__lists ul.filters-button-group li").removeClass('is-checked');			
			jQuery(this).addClass('active');
			jQuery(this).parent().addClass('is-checked');
		});
	});

	var pageNumber = 4; 
	var loading  = false; 
	var gppp = 3;
	var ajax_running = 0;
	function load_contents(newscat,obj) {
		if(loading == false){
			loading = true;
			jQuery('.loading-info').show();
			if(obj == 'true'){
				var dataString  = {
					'postcat': newscat,
					'action': 'loadmorepost'  
				};
			} else {
				var dataString  = {
					'pageNumber': pageNumber,
					'ppp': gppp,
					'postcat': newscat,
					'action': 'loadmorepost'  
				};
			}
			ajax_running++;
			if(ajax_running == 1){				
				jQuery('.load-more-img').show();
				var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>';
				jQuery.ajax({
					type: "POST",
					url: ajaxurl,
					data: dataString,
					success: function(data){
						jQuery('.load-more-img').hide();
						pageNumber++;
						loading = false;
						if(jQuery.trim(data) != '') {
							var json_obj = jQuery.parseJSON(data);
							if(json_obj["data"] != '') {
								jQuery('.show-more-data .no-post').hide();
								if(json_obj["count"] >= 9) {
									jQuery('a.more_posts').show();
								} else { 
									jQuery('a.more_posts').hide();
								}

								if(obj == 'true'){
									jQuery(".grid").html(json_obj["data"]);
								} else {
									jQuery(".grid").append(json_obj["data"]);
								}
							} else {
								if(obj == 'true'){
								  	jQuery(".grid").html(json_obj["data"]);
								} else {
								  	jQuery(".grid").append(json_obj["data"]);
								}
								jQuery('.show-more-data .no-post').show();
								jQuery('a.more_posts').hide();
            				}
        				} 
        				ajax_running = 0;
    				}
				});
			}
		}
		return false;
	}
	/*Load More Button*/
	jQuery(document).on("click", "a.more_posts", function() {        
        var obj = '';
		loading = false;
		var newscat = jQuery("ul.filters-button-group li a.active").attr('data-value');
		if(newscat != '') { 
			newscat = newscat; 
		} else { 
			newscat = 'All'; 
		}
		load_contents(newscat,obj);
    });
	/*Filter*/
	jQuery('ul.filters-button-group li a').on('click', function() {
		pageNumber = 3;
		var obj    = 'true';
		loading    = false;
		var newscat =  jQuery(this).attr('data-value');
		load_contents(newscat,obj);
	});
</script>