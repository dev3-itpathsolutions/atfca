jQuery(document).ready(function(e){
	jQuery('.gallery__slider').owlCarousel({
		items: 1,
		autoplay: true,
		loop: true,
		nav: true,
		smartSpeed: 2500,
		margin: 0
	});
	

	jQuery(".menu__toggle").on('click', function() { 
		jQuery(this).toggleClass("on");
		jQuery('.menu__section').toggleClass("on");
		jQuery('.header__section').toggleClass("on");
		jQuery(".nav").toggleClass('hidden');
	});
    jQuery('.on').removeClass('header__bg');

    jQuery('.overview-block-services h3').click(function(){
    	jQuery(this).parents('.overview-block-services').toggleClass('open');
    }); 

	jQuery(".faq__item .faqsec").click(function (e) {
		jQuery(this).toggleClass("open");
		jQuery(this).next(".faqans").slideToggle(300);
	});
});
	jQuery(window).scroll(function() {    
	    var scroll = jQuery(window).scrollTop();
	    if (scroll >= 100) {
	        jQuery(".header__section").addClass("header__bg");
	    } else {
	        jQuery(".header__section").removeClass("header__bg");
	    }
	});

jQuery(window).on("load",function(){
	checkPosition();
});
function checkPosition() {
	if (window.matchMedia('(max-width: 1024px)').matches) {
		jQuery('.nav > li > a').bind("click touchend", function(e){
		    var el = jQuery(this);
		    var link = el.attr('href');
		    window.location = link;
		});

		jQuery('.nav > li.menu-item-has-children').append('<span class="carets"></span>');
		jQuery('.nav li.mega-menu-column.menu-item-has-children').append('<span class="newcarets"></span>');

		jQuery('.nav > .menu-item-has-children .carets').bind("click touchstart", function(e){
		  	e.preventDefault();
		  	var $this = jQuery(this);
		  	if($this.parent().hasClass('open')) {
		  		$this.parent().removeClass('open');	
		  	} else { 
			    jQuery('.nav > li.menu-item-has-children').removeClass('open');
				$this.parent().toggleClass('open');
			}
		});

	} 
}
