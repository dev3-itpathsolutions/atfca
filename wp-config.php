<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_atfca' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '&WK+GqGBYbUwQwOSVRs>GF>vTwjej){UP,Oy~0i<@{6TF;b@ML,++jK_K|t#bI0p' );
define( 'SECURE_AUTH_KEY',  'xn?PU,pn)C}*raU0amYC,=OCdG T/75s:20)T)B{88W(scw%K#8c?R}JtcIRWlDS' );
define( 'LOGGED_IN_KEY',    '6SOThhd3:v1kgX<(lZ~o.Au<Aux:gK^)|tcqeJD|ke}#&9ZAMyS=6>_[@YZ Npof' );
define( 'NONCE_KEY',        '_c|59Kij$XWJ..W_+6[0hJ$mO[p/pIjTE]/{|F@:sC[<[[WN@VYdoEG_raxtC-i$' );
define( 'AUTH_SALT',        'Cp=j>a?DgtfD:z8`9:f^e m-b<%G9f_,e)=f$h%==l-0av5lV1qS)d-c*tDl4f*I' );
define( 'SECURE_AUTH_SALT', '>,FD&9yWb`&>$CFkjb7#1kRF;zoL:5q f5m3XtnThRUG#FwxxtzINbM8fx3<L;W,' );
define( 'LOGGED_IN_SALT',   '*-P{I .>U.(!C)u_R<yEG&LK*!) ;B9NiaKw(7&KQNH(;7uAjO#;+7{_([yG]3X_' );
define( 'NONCE_SALT',       'ER}H4gtn=nTw4Wm699z(O{y<XO=S4;kj%_khy mUrF<r?>!nQTWjbxe/}3=$AXSb' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
