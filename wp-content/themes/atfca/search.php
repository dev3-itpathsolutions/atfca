<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package ATFCA
 */

get_header();
?>
<header class="slider__section inner__section inline__block">
	<div class="container tbl">
		<div class="tbl-cell">
			<div class="slider__content">
				<h2>Search</h2>
			</div>
		</div>
	</div>
	<div class="bgoverlay" style="background-image: url('<?php echo get_template_directory_uri();?>/images/constitutions-banner.jpg');"></div>
	<div class="gredientoverlay" style="background: rgba(0, 0, 0, 0.396003);"></div>
</header>
<section class="default-page-sec">
    <div class="container">
		<?php 
		if ( have_posts() ) : ?>				
			<h3 class="page-title">	<?php printf( esc_html__( 'Search Results for: %s', 'atfca' ), '<span>' . get_search_query() . '</span>' );
				?>
			</h3>
			<?php
			while ( have_posts() ) :the_post();
				get_template_part( 'template-parts/content', 'search' );
			endwhile;
			the_posts_navigation();
		else :
			get_template_part( 'template-parts/content', 'none' );
		endif;	?>
    </div>
</section>

<?php
get_footer();
