<?php
/**
 * ATFCA functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ATFCA
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'atfca_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function atfca_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on ATFCA, use a find and replace
		 * to change 'atfca' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'atfca', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		/*register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'atfca' ),
			)
		);*/

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'atfca_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'atfca_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function atfca_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'atfca_content_width', 640 );
}
add_action( 'after_setup_theme', 'atfca_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function atfca_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'atfca' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'atfca' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'atfca_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function atfca_scripts() {
	wp_enqueue_style( 'atfca-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'atfca-style', 'rtl', 'replace' );

	wp_enqueue_script( 'atfca-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'atfca_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
* Load function file.
*/
require_once('functions/admin.php');
require_once('functions/menu.php');
require_once('functions/default.php');
require_once('functions/debug.php');
require_once('functions/acf.php');
require_once('functions/cpt.php');
require_once('functions/image.php');
require_once('functions/enqueue.php');
require_once('functions/woo.php');


/********** Load more functionaly of Products ajax function ***********/
add_action('wp_ajax_nopriv_loadmoreproducts', 'loadmoreproducts');
add_action('wp_ajax_loadmoreproducts', 'loadmoreproducts');
function loadmoreproducts(){
	$gppp = (isset($_POST["ppp"])) ? $_POST["ppp"] : 1;
	$page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;
	$productcat = (isset($_POST['productcat'])) ? $_POST['productcat'] : '';
	$obj = (isset($_POST['obj'])) ? $_POST['obj'] : '';
	$sorting = (isset($_POST['sortingval'])) ? $_POST['sortingval'] : '';

	header("Content-Type: text/html");
	wp_reset_query();
	$responses = array();
	
	$args['tax_query'] = array();
	if($obj == 'true') {		
		$args = array(
		    'post_type' => 'product',
		    'post_status' => 'publish',
		    'posts_per_page' => 12,
		    'orderby' => 'date'
		);		
    } else {
        $args = array(
            'post_type' => 'product',
			'posts_per_page' => $gppp,
			'post_status' => 'publish',
			'paged' => intval($page),
			'orderby' => 'date',
            'order' => 'DESC',
        );
    }

    if(!empty($productcat)) {
    	$args['tax_query']['relation'] = 'OR';
        $cattaxonomy = array(
            'taxonomy' => 'product_cat',
            'field' => 'slug',
            'terms' => $productcat,
            'operator' => 'IN'
        );
        array_push($args['tax_query'],$cattaxonomy );
    }

    if($sorting == 'date') {
    	$args['orderby'] = 'date';
    	$args['order'] = 'DESC';
    } else if($sorting == 'popularity') {
    	$args['orderby'] = 'meta_value_num';
    	$args['order'] = 'DESC';
    	$args['meta_key'] = 'total_sales';
    } else if($sorting == 'rating') {
    	$args['orderby'] = 'meta_value_num';
    	$args['order'] = 'DESC';
    	$args['meta_key'] = '_wc_average_rating';
    } else if($sorting == 'price') {
    	$args['orderby'] = 'meta_value_num';
    	$args['order'] = 'ASC';
    	$args['meta_key'] = '_price';
    } else if($sorting == 'price-desc') {
    	$args['orderby'] = 'meta_value_num';
    	$args['order'] = 'DESC';
    	$args['meta_key'] = '_price';
    } else {
    	$args['orderby'] = 'date';
    	$args['order'] = 'DESC';
    }

	$loop = new wp_Query($args);
	$postcount = $loop->found_posts;
	$html = '';
	global $product;
	if ( $loop->have_posts() ) :
		$count = 1;
		while ( $loop->have_posts() ) : $loop->the_post();
        		do_action( 'woocommerce_shop_loop' );
				wc_get_template_part( 'content', 'product' );
            ?>            
        <?php
        endwhile;
    endif;
    wp_reset_postdata();
    exit();
}