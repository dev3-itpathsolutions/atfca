<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ATFCA
 */

?>

<?php 
if(!is_page( 'join-now' )):
 	get_template_part('template-parts/join','now' );
endif;?>


<?php 
$telNumber 	= get_field('footer_telephone','option');
$Email 		= get_field('footer_email','option');
$joinButton = get_field('join_now_button','option');
$memLogin 	= get_field('header_member_login','option');
$copyright 	= get_field('copy_right','option');
$footerLogo = get_field('footer_logo','option');
if(empty($footerLogo)){
	$footerLogo['url'] = get_template_directory_uri().'/images/logo.svg';
	$footerLogo['alt'] = "logo";
}?>
<section class="footer__sect inline__block paddingtop">
	<div class="container">
		<div class="container__inner">
			<div class="footer__inner inline__block">
				<div class="footer__main logo__sect">
					<a href="<?php echo get_site_url();?>">
						<img src="<?php echo $footerLogo['url'];?>" alt="<?php echo $footerLogo['alt'];?>">
					</a>
				</div>
				<div class="footer__main shop__menu">
					<?php 
					if(!empty($telNumber) || !empty($telNumber)) {?>
						<h3>Contact</h3>
						<ul>
							<?php if(!empty($telNumber)){?>
								<li>T: <a href="tel:<?php echo $telNumber;?>"><?php echo $telNumber;?></a></li>
							<?php } 
							if(!empty($Email)){	?>
								<li>E: <a href="mailto:<?php echo $Email;?>"><?php echo $Email;?></a></li>
							<?php }?>
						</ul>
					<?php }

					$facebook 	= get_field('facebook','option');
					$twitter 	= get_field('twitter','option');
					$googleplus = get_field('google_plus','option');
					if(!empty($facebook['url']) || !empty($googleplus['url']) || !empty($twitter['url'])){ ?>

						<div class="social__icons inline__block">
							<ul class="share-buttons">
								<?php 
								if(!empty($facebook['url'])) { ?>
									<li><a href="<?php echo $facebook['url'];?>" target="_blank" rel="noreferrer"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<?php } 
								if(!empty($twitter['url'])) {?>
									<li><a href="<?php echo $twitter['url'];?>" target="_blank" rel="noreferrer"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<?php }
								if(!empty($googleplus['url'])) {?>
									<li><a href="<?php echo $googleplus['url'];?>" target="_blank" rel="noreferrer"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
								<?php } ?>
							</ul>      
						</div>
					<?php } 

					if(!empty($memLogin['url']) && !empty($memLogin['title']) ) { ?>
						<a class="cmnbtn" href="<?php echo $memLogin['url']; ?>" <?php echo ($memLogin['target'] ? 'target="_blank" rel="noreferrer" ' : ''); ?>><?php echo $memLogin['title']; ?><span class="gg-shape-triangle"></span></a>	
					<?php } ?>
				</div>
				<div class="footer__main info__menu">
					<ul id="menu-footer-information-menu" class="">
						<?php
						wp_nav_menu( 
							array(
								'menu'            => '',
								'theme_location'  => 'footer-menu',
								'container'       => 'false',
								'menu_class'      => '',
								'echo'            => true,
								'items_wrap'      => '%3$s'
							)
						); ?>
					</ul>        
				</div>
				
				<?php wp_reset_query(); if( have_rows('add_certification','option') ):?>
					<div class="footer__main follow__menu">
						<h3>Certification</h3>
						<ul>
							<?php 
							while ( have_rows('add_certification','option') ) : the_row();
								$cerImage = get_sub_field('image','option'); ?>
								<li><img src="<?php echo $cerImage['url']; ?>" class="img-responsive"></li>
							<?php endwhile;?>
						</ul>
					</div>
				<?php endif;?>
			</div>
			<div class="copyright inline__block">
				<div class="footer_btm">
					<?php echo $copyright;?>
				</div>
				<div class="footer_btm">
					<p>Built by: <a href="https://thriveweb.com.au/" target="_blank" rel="noreferrer">Thrive Digital</a></p>
				</div>
			</div>
		</div>
	</div>
</section>
<?php wp_footer(); ?>

</body>
</html>
