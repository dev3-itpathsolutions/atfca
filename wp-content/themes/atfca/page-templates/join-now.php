<?php
/**
 * Template Name: join Now Template
 */
get_header(); ?>

<?php get_template_part('template-parts/page','banner' );?>

<?php 
$introSMTitle   = get_field('join_now_intro_small_title');
$introTitle 	= get_field('join_now_intro_title');
$introDesc 		= get_field('join_now_intro_description');
?>
<section class="joinnow__sect tacenter common__sect inline__block paddingtop">
	<div class="container">
		<div class="container__inner">
			<span class="tagline"><?php echo $introSMTitle;?></span>
			<h3><?php echo $introTitle;?></h3>
			<?php echo $introDesc;

			$planProducts = get_field('add_product');
			if($planProducts){
			?>
			<div class="flex">
				<?php 
				foreach( $planProducts as $planProduct): 
				global $woocommerce;
				$_product = wc_get_product( $planProduct->ID );
				$cart_page_url = function_exists( 'wc_get_cart_url' ) ? wc_get_cart_url() : $woocommerce->cart->get_cart_url();
				$content = wpautop( $planProduct->post_content );
				$price_html = $_product->get_price_html(); ?>
					<div class="overview-block-product">
						<h3><?php echo $planProduct->post_title;?></h3>
						<div class="join__details inline__block">
							<?php echo $content ;?>
							<span class="price"><?php echo $price_html;?></span>
							<a href="<?php echo $cart_page_url;?>?add-to-cart=<?php echo $planProduct->ID;?>" class="cmnbtn">Buy Now <span class="gg-shape-triangle"></span></a>
						</div>
					</div>
				<?php endforeach;?>
			</div>
		<?php } ?>
		</div>
	</div>
</section>

<?php 
$ourpages = get_field('join_now_page_add');
if($ourpages){
	$ourSmallTitle = get_field('join_now_page_small_title');
	$ourTitle 	   = get_field('join_now_page_title');
?>
<section class="home_services constitution__services common__sect inline__block paddingtop">
	<div class="container">
		<div class="container__inner">
			<div class="common__info tacenter">
				<?php if(!empty($ourSmallTitle)){?>
					<span class="tagline"><?php echo $ourSmallTitle;?></span>
				<?php } ?>
				<h2><?php echo $ourTitle;?></h2>
			</div>
			<div class="flex">
				<?php 
				foreach( $ourpages as $oursPage):
					$oursPageIMG	= get_the_post_thumbnail_url($oursPage->ID);
					if(empty($oursPageIMG)){
						$oursPageIMG = get_template_directory_uri().'/images/placeholder.png';
					}?>
					<div class="overview-block-product">
						<a href="<?php echo get_permalink($oursPage->ID);?>">
							<div class="tbl">
								<div class="tbl-cell">
									<h3><?php echo $oursPage->post_title;?></h3>
								</div>
							</div>
							<div class="bgoverlay" style="background-image: url('<?php echo $oursPageIMG;?>');"></div>
							<div class="gredientoverlay" style="background: linear-gradient(180deg, #00212121, #05212121, #212121);"></div>
						</a>
					</div>
				<?php endforeach; wp_reset_postdata();?>
			</div>
		</div>
	</div>
</section>
<?php } ?>


<?php 
$cousmTitle	= get_field('join_now_cour_top_title');
$couTitle 	= get_field('join_now_cour_title');
$couButton	= get_field('join_now_cour_button');
$couImage	= get_field('join_now_cour_image');
if(empty($couImage)){
	$couImage['url'] = get_template_directory_uri().'/images/LJ-Stratton1-vg.jpg';
} ?>
<section class="courses__sect common__sect inline__block paddingtop" style="background-image: url('<?php echo $couImage['url'];?>');">
	<div class="container">
		<div class="container__inner tbl">
			<div class="tbl-cell">
				<h2><?php echo $cousmTitle;?></h2>
				<h3><?php echo $couTitle;?></h3>
				<?php
				if(!empty($couButton['url']) && !empty($couButton['title']) ) { ?>
					<a href="<?php echo $couButton['url']; ?>" <?php echo ($couButton['target'] ? 'target="_blank" rel="noreferrer" ' : ''); ?> class="cmnbtn"><?php echo $couButton['title']; ?><span class="gg-shape-triangle"></span></a>
				<?php }?>
			</div>
			<div class="square__img">
				<img src="<?php echo get_template_directory_uri();?>/images/courses-square.png">
			</div>
		</div>
	</div>
	<div class="courses__right">
		<img src="<?php echo get_template_directory_uri();?>/images/ATFCAmap2.png">
	</div>
	<div class="courses__left">
		<img src="<?php echo get_template_directory_uri();?>/images/courses-left.png">
	</div>
	<div class="gredientoverlay" style="background: linear-gradient(270deg, #CB212121, #66212121, #212121);"></div>
</section>
<?php get_footer(); ?>
