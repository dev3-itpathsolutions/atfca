<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ATFCA
 */

get_header();
?>

<?php get_template_part('template-parts/page','banner' );?>

<?php
if( have_rows('default_section') ):
        while ( have_rows('default_section') ) : the_row();
        	if ( get_row_layout() == 'dp_intro_section' ): /* Intro section */ 
        		$introSMTitle 	= get_sub_field('dp_intro_small_title');
                $introTitle 	= get_sub_field('dp_intro_title');
                $introDesc 		= get_sub_field('description');	?>
				<section class="constitutions__sect tacenter common__sect inline__block paddingtop">
					<div class="container">
						<div class="container__inner">
							<span class="tagline"><?php echo $introSMTitle;?></span>
							<h3><?php echo $introTitle;?></h3>
							<?php echo $introDesc; ?>
						</div>
					</div>
				</section>

			<?php /* According section */
            elseif( get_row_layout() == 'dp_accordion_section' ): 
            	$acTitle = get_sub_field('dp_accordion_title');
            	$button1 = get_sub_field('dp_accordion_button_1');
            	$button2 = get_sub_field('dp_accordion_button_2');
            	if( have_rows('dp_add_accordion') ):?>
					<section class="const__faqs common__sect inline__block paddingtop">
						<div class="container">
							<div class="container__inner">
								<div class="common__info tacenter">
									<h2><?php echo $acTitle;?></h2>
								</div>
								<div class="faq__list inline__block">
									<?php 
									while ( have_rows('dp_add_accordion') ) : the_row();
	                                $tabTitle 	= get_sub_field('title');
	                                $tabDesc 	= get_sub_field('description');?>
										<div class="faq__item inline__block">
											<div class="faqsec"><?php echo $tabTitle;?></div>
											<div class="faqans">
												<?php echo $tabDesc;?>
											</div>
										</div>
									<?php endwhile;?>
								</div>
								<div class="faq__btns tacenter inline__block">
									<?php
									if(!empty($button1['url']) && !empty($button1['title']) ) { ?>
										<a href="<?php echo $button1['url']; ?>" <?php echo ($button1['target'] ? 'target="_blank" rel="noreferrer" ' : ''); ?> class="cmnbtn"><?php echo $button1['title']; ?><span class="gg-shape-triangle"></span></a>
									<?php }?>
									<?php
									if(!empty($button2['url']) && !empty($button2['title']) ) { ?>
										<a href="<?php echo $button2['url']; ?>" <?php echo ($button2['target'] ? 'target="_blank" rel="noreferrer" ' : ''); ?> class="cmnbtn"><?php echo $button2['title']; ?><span class="gg-shape-triangle"></span></a>
									<?php }?>
								</div>
							</div>
						</div>
					</section>
				<?php endif;?>

			<?php /* Two Column section */
            elseif( get_row_layout() == 'dp_two_col_section' ): 
            	$colSMTitle = get_sub_field('dp_two_col_small_title');
            	$colTitle 	= get_sub_field('dp_two_col_title');
            	$colDesc 	= get_sub_field('dp_two_col_description');
            	$colButton 	= get_sub_field('dp_two_col_button');
            	$colImage  	= get_sub_field('dp_two_col_image');
            	if(empty($colImage['url'])){
					$colImage['url'] = get_template_directory_uri().'/images/placeholder.png';
				}?>
            	<section class="home_mdl_sect coaches__bg common__sect inline__block paddingtop">
            		<div class="container">
            			<div class="container__inner">
            				<div class="home_mdl_left">
            					<?php if(!empty($colSMTitle)){?>
            						<span class="tagline"><?php echo $colSMTitle;?></span>
            					<?php } ?>
            					<h3><?php echo $colTitle;?></h3>
            					<?php echo $colDesc;
            					
								if(!empty($colButton['url']) && !empty($colButton['title']) ) { ?>
									<a href="<?php echo $colButton['url']; ?>" <?php echo ($colButton['target'] ? 'target="_blank" rel="noreferrer" ' : ''); ?> class="cmnbtn"><?php echo $colButton['title']; ?><span class="gg-shape-triangle"></span></a>
								<?php } ?>
            				</div>
            				<div class="home_mdl_right">
            					<img src="<?php echo $colImage['url'];?>" alt="image" class="img-responsive">
            				</div>
            			</div>
            		</div>
            	</section>

			<?php /*Help section */
            elseif( get_row_layout() == 'dp_pages_section' ):
	            	$helpSMTitle 	= get_sub_field('dp_pages_small_title');
	                $helpTitle 		= get_sub_field('dp_pages_title');
	                $helpPages 		= get_sub_field('dp_page');
            		if($helpPages): ?>
					<section class="home_services constitution__services common__sect inline__block paddingtop">
						<div class="container">
							<div class="container__inner">
								<div class="common__info tacenter">
									<span class="tagline"><?php echo $helpSMTitle;?></span>
									<h2><?php echo $helpTitle;?></h2>
								</div>
								<div class="flex">
									<?php 
									foreach( $helpPages as $helpPage): 
										$helpIMG  = get_the_post_thumbnail_url($helpPage->ID);
										if(empty($helpIMG)){
											$helpIMG = get_template_directory_uri().'/images/placeholder.png';
										}?>
										<div class="overview-block-product">
											<a href="<?php echo get_permalink($helpPage->ID);?>">
												<div class="tbl">
													<div class="tbl-cell"><h3><?php echo $helpPage->post_title;?></h3></div>
												</div>
												<div class="bgoverlay" style="background-image: url('<?php echo $helpIMG;?>');"></div>
												<div class="gredientoverlay" style="background: linear-gradient(180deg, #00212121, #05212121, #212121);"></div>
											</a>
										</div>
									<?php endforeach;?>
								</div>
							</div>
						</div>
					</section>
					<?php endif;?>

			<?php /* News Section */
            elseif( get_row_layout() == 'dp_news_section' ):
                $newsSMTitle = get_sub_field('dp_news_small_title');
                $newsTitle 	 = get_sub_field('dp_news_title');
                $addNews 	 = get_sub_field('add_news');
            	if($addNews): ?>
					<section class="state__news common__sect inline__block paddingtop">
						<div class="container">
							<div class="container__inner">
								<div class="common__info tacenter">
									<span class="tagline">articles</span>
									<h2>Related news</h2>
								</div>
								<div class="flex">
									<?php 
									foreach( $addNews as $news):
										$cat_list 	= get_the_terms( $news->ID, 'category' );
										$cat_string = "";
										foreach ( $cat_list as $term ) {
											if($term->name != "Uncategorized"){
												$cat_string .= $term->name."  ";
											}
										}
										$newDescr = get_sub_field('add_news');
										$newsIMG  = get_the_post_thumbnail_url($news->ID);
										if(empty($newsIMG)){
											$newsIMG = get_template_directory_uri().'/images/placeholder.png';
										}?>
										<div class="overview-block-product">
											<a href="<?php echo get_permalink($news->ID);?>">
												<div class="shopbg bgproperty" style="background-image: url('<?php echo $newsIMG;?>');"></div>
												<div class="state_details inline__block">
													<div class="category"><?php echo $cat_string;?></div>
													<h3><?php echo get_the_title($news->ID);?></h3>
													<p><?php echo get_the_excerpt($news->ID);?></p>
													<span class="cmnbtn">Read more <span class="gg-shape-triangle"></span></span>
												</div>
											</a>
										</div>
									<?php endforeach;?>
								</div>
							</div>
						</div>
					</section>
				<?php endif;
			endif;
		endwhile;
	else:
		// Default page content  -->
		if(have_posts()):
		while ( have_posts() ) : the_post();
			?>
			<section class="default-page-sec">
		        <div class="container">
		            <?php the_content();?>
		        </div>
		    </section>

		<?php endwhile; endif;
endif; 


get_footer();
