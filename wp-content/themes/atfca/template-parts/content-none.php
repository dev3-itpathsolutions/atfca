<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ATFCA
 */

?>

<h2 class="page-title"><?php esc_html_e( 'Nothing Found', 'tactical-gear-experts' ); ?></h2>
<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'tactical-gear-experts' ); ?></p>

