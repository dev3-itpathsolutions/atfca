<?php 
/*
* This template page is join now section
*/

$joinBoldTitle = get_field('join_now_top_title','option');
$joinTitle = get_field('join_now_title','option');
$joinButton = get_field('join_now_button','option');
$joinImage = get_field('join_now_image','option');
if(empty($joinImage)){
	$joinImage['url'] = get_template_directory_uri().'/images/join-now.jpg';
}?>
<section class="join_nowsect common__sect inline__block paddingtop">
	<div class="container">
		<div class="container__inner tbl">
			<div class="tbl-cell">
				<h3><?php echo $joinBoldTitle;?></h3>
				<h4><?php echo $joinTitle;?></h4>
				<?php
				if(!empty($joinButton['url']) && !empty($joinButton['title']) ) { ?>
					<a href="<?php echo $joinButton['url']; ?>" <?php echo ($joinButton['target'] ? 'target="_blank" rel="noreferrer" ' : ''); ?> class="cmnbtn"><?php echo $joinButton['title']; ?><span class="gg-shape-triangle"></span></a>
				<?php }?>
			</div>
		</div>
	</div>
	<div class="bgoverlay" style="background-image: url('<?php echo $joinImage['url'];?>');"></div>
	<div class="gredientoverlay" style="background: linear-gradient(180deg, #00212121, #212121);"></div>
</section>