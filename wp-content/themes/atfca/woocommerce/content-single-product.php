<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
$cat_list = get_the_terms( get_the_ID(), 'product_cat' );
if(!empty($cat_list)){
	$cat_list  = join(', ', wp_list_pluck($cat_list, 'name'));
}
$price 	= $product->get_price_html();
$desc   = $product->get_short_description();
?>

<section class="shop__single inline__block">
	<div class="container">
		<div class="container__inner">
			<?php 
			do_action( 'woocommerce_before_single_product' );
			?>
			<div class="flex">				
				<div class="woo__details">
					<h1 class="product_title entry-title"><?php the_title(); ?></h1> 
					<span class="tagline"><?php echo $cat_list;?></span>
					<p class="price"><?php echo $price;?></p>
					<div class="woocommerce-product-details__short-description">
						<?php echo $desc;?>
					</div>
					<?php woocommerce_template_single_add_to_cart();?>
					<a href="<?php echo get_permalink( get_page_by_path( 'shipping-info' ) );?>" class="cmnbtn">Shipping info</a>
				</div>
				<div class="woo__gallery">
		          	<?php echo do_action( 'woocommerce_before_single_product_summary' );?>
		        </div>
			</div>
			<?php do_action( 'woocommerce_after_single_product' ); ?>
		</div>
	</div>
</section>
<?php 
$tab1 		= get_the_content();
$tab2Title 	= get_field("product_tab_2_title");
$tab2desc 	= get_field("product_tab_2_desc");
$tab3Title 	= get_field("product_tab_3_title");
$tab3desc 	= get_field("product_tab_3_desc");
?>
<section class="const__faqs shop_faqs common__sect inline__block paddingtop">
	<div class="container">
		<div class="container__inner">
			<div class="common__info tacenter">
				<h2>More ABout The Product</h2>
			</div>
			<div class="faq__list inline__block">
				<?php if(!empty($tab1)){?>
					<div class="faq__item inline__block">
						<div class="faqsec">Product Details</div>
						<div class="faqans">
							<?php the_content();?>
						</div>
					</div>
				<?php } 

				if(!empty($tab2Title)){ ?>
				<div class="faq__item inline__block">
					<div class="faqsec"><?php echo $tab2Title;?></div>
					<div class="faqans">
						<?php echo $tab2desc;?>
					</div>
				</div>
			<?php } 
			if(!empty($tab3Title)){ ?>
				<div class="faq__item inline__block">
					<div class="faqsec"><?php echo $tab3Title;?></div>
					<div class="faqans">
						<?php echo $tab3desc;?>
					</div>
				</div>
			<?php } ?>
			</div>
			<div class="social__share tacenter inline__block">
				<ul>
					<li>Share on:</li>
					<li><a target="_blank" href="http://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink();?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a target="_blank" href="http://twitter.com/intent/tweet/?text=<?php echo get_the_title();?>&url=<?php echo get_the_permalink();?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a target="_blank" href="https://plus.google.com/share?url=<?php echo get_the_permalink();?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="shop__online common__sect inline__block paddingtop">
	<div class="container">
		<div class="container__inner">
			<div class="common__info tacenter">
				<span class="tagline">You might also like</span>
			</div>
			<ul class="products columns-3">
				  <?php echo woocommerce_output_related_products(); ?>
				<div class="shop_div inline__block tacenter">
					<a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>" class="cmnbtn">shop all products <span class="gg-shape-triangle"></span></a>
				</div>
			</div>
		</div>
</section>