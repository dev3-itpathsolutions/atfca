<?php 
$bannerIMG     = get_the_post_thumbnail_url();
if(empty($bannerIMG)){
	$bannerIMG = get_template_directory_uri().'/images/constitutions-banner.jpg';
}?>
<header class="slider__section inner__section inline__block">
	<div class="container tbl">
		<div class="tbl-cell">
			<div class="slider__content">
				<h2><?php echo get_the_title(); ?></h2>
			</div>
		</div>
	</div>
	<div class="bgoverlay" style="background-image: url('<?php echo $bannerIMG; ?>');"></div>
	<div class="gredientoverlay" style="background: rgba(0, 0, 0, 0.396003);"></div>
</header>