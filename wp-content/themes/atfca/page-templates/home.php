<?php
/**
 * Template Name: Home Template
 */
get_header(); ?>

<?php 
$bannerTitle   = get_field('home_banner_title');
$bannerBoldTitle = get_field('home_banner_bold_title');
$bannerButton  = get_field('banner_button');
$bannerIMG     = get_the_post_thumbnail_url();
if(empty($bannerIMG)){
	$bannerIMG = get_template_directory_uri().'/images/home-banner.jpg';
}?>
<header class="slider__section inline__block">
	<div class="container tbl">
		<div class="tbl-cell">
			<div class="slider__content">
				<h2><?php echo $bannerTitle;?> <span class="small__text"><?php echo $bannerBoldTitle;?></span></h2>
				<?php 
				if(!empty($bannerButton['url']) && !empty($bannerButton['title']) ) { ?>
					<a href="<?php echo $bannerButton['url']; ?>" <?php echo ($bannerButton['target'] ? 'target="_blank" rel="noreferrer" ' : ''); ?> class="cmnbtn"><?php echo $bannerButton['title']; ?><span class="gg-shape-triangle"></span></a>
				<?php } ?>
			</div>
			<div class="square__img">
				<img src="<?php echo get_template_directory_uri();?>/images/square.png">
			</div>
		</div>
	</div>
	<div class="bgoverlay" style="background-image: url('<?php echo $bannerIMG;?>');"></div>
</header>

<?php 
$abSmallTitle   = get_field('home_ab_small_title');
$abTitle 		= get_field('home_ab_title');
$abDescr  		= get_field('home_ab_description');
$abButton		= get_field('home_ab_button');
$abLeftImg		= get_field('home_ab_image_left');
$abRightImg		= get_field('home_ab_image_right');
?>
<section class="home_mdl_sect common__sect inline__block paddingtop">
	<div class="container">
		<div class="container__inner">
			<div class="home_mdl_left">
				<span class="tagline"><?php echo $abSmallTitle;?></span>
				<h3><?php echo $abTitle;?></h3>
				<?php echo $abDescr;
				if(!empty($abButton['url']) && !empty($abButton['title']) ) { ?>
					<a href="<?php echo $abButton['url']; ?>" <?php echo ($abButton['target'] ? 'target="_blank" rel="noreferrer" ' : ''); ?> class="cmnbtn"><?php echo $abButton['title']; ?><span class="gg-shape-triangle"></span></a>
				<?php } ?>
			</div>
			<div class="home_mdl_right">
				<div class="left__img">
					<img src="<?php echo $abLeftImg['url'];?>">
				</div>
				<div class="right__img">
					<img src="<?php echo $abRightImg['url'];?>">
				</div>
			</div>
		</div>
	</div>
</section>

<?php 
$ourservices = get_field('home_our_page');
if($ourservices){
	$osSmallTitle = get_field('home_os_small_title');
	$osTitle 	  = get_field('home_os_title');
?>
	<section class="home_services common__sect inline__block paddingtop">
		<div class="container">
			<div class="container__inner">
				<div class="common__info tacenter">
					<span class="tagline"><?php echo $osSmallTitle;?></span>
					<h2><?php echo $osTitle;?></h2>
				</div>
				<div class="flex">
					<?php 
					foreach( $ourservices as $oursPage): setup_postdata($oursPage); 
						$serviceIMG     = get_the_post_thumbnail_url($oursPage->ID);
						if(empty($serviceIMG)){
							$serviceIMG = get_template_directory_uri().'/images/placeholder.png';
						}?>
						<div class="overview-block-product">
							<a href="<?php echo get_permalink($oursPage->ID);?>">
								<div class="tbl">
									<div class="tbl-cell">
										<h3><?php echo $oursPage->post_title;?></h3>
									</div>
								</div>
								<div class="bgoverlay" style="background-image: url('<?php echo $serviceIMG;?>');"></div>
								<div class="gredientoverlay" style="background: linear-gradient(180deg, #00212121, #05212121, #212121);"></div>
							</a>
						</div>
					<?php endforeach; wp_reset_postdata();?>
				</div>
			</div>
		</div>
	</section>
<?php } ?>

<?php 
$obSmallTitle   = get_field('home_objective_small_title');
$obTitle 		= get_field('home_objective_title');
$obDescr  		= get_field('home_objective_description');
$obButton1		= get_field('home_objective_button_1');
$obButton2		= get_field('home_objective_button_2');
$obImage		= get_field('home_objective_image');
if(empty($obImage)){
	$obImage['url'] = get_template_directory_uri().'/images/LJ-Stratton1-vg.jpg';
} ?>
<section class="objective__sect common__sect inline__block paddingtop">
	<div class="container">
		<div class="container__inner">
			<div class="flex">
				<div class="objective__left">
					<div class="objective__img bgproperty" style="background-image: url('<?php echo $obImage['url'];?>');">
						<div class="square__img">
							<img src="<?php echo get_template_directory_uri();?>/images/objective-square.png">
						</div>
					</div>
				</div>
				<div class="objective__left">
					<div class="objective__content">
						<span class="tagline"><?php echo $obSmallTitle; ?></span>
						<h3><?php echo $obTitle;?></h3>
						<?php echo $obDescr; ?>
						<?php
						if(!empty($obButton1['url']) && !empty($obButton1['title']) ) { ?>
							<a href="<?php echo $obButton1['url']; ?>" <?php echo ($obButton1['target'] ? 'target="_blank" rel="noreferrer" ' : ''); ?> class="cmnbtn"><?php echo $obButton1['title']; ?><span class="gg-shape-triangle"></span></a>
						<?php }
						if(!empty($obButton2['url']) && !empty($obButton2['title']) ) { ?>
							<a href="<?php echo $obButton2['url']; ?>" <?php echo ($obButton2['target'] ? 'target="_blank" rel="noreferrer" ' : ''); ?> class="cmnbtn"><?php echo $obButton2['title']; ?><span class="gg-shape-triangle"></span></a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php 
$couTitle  	= get_field('home_cou_top_title');
$couBolTitle 	= get_field('home_cou_totitle');
$couButton	= get_field('home_cou_button');
$couImage	= get_field('home_cou_image');
if(empty($couImage)){
	$couImage['url'] = get_template_directory_uri().'/images/LJ-Stratton1-vg.jpg';
} ?>
<section class="courses__sect common__sect inline__block paddingtop" style="background-image: url('<?php echo $couImage['url'];?>');">
	<div class="container">
		<div class="container__inner tbl">
			<div class="tbl-cell">
				<h2><?php echo $couTitle;?></h2>
				<h3><?php echo $couBolTitle;?></h3>
				<?php
				if(!empty($couButton['url']) && !empty($couButton['title']) ) { ?>
					<a href="<?php echo $couButton['url']; ?>" <?php echo ($couButton['target'] ? 'target="_blank" rel="noreferrer" ' : ''); ?> class="cmnbtn"><?php echo $couButton['title']; ?><span class="gg-shape-triangle"></span></a>
				<?php }?>
			</div>
			<div class="square__img">
				<img src="<?php echo get_template_directory_uri();?>/images/courses-square.png">
			</div>
		</div>
	</div>
	<div class="courses__right">
		<img src="<?php echo get_template_directory_uri();?>/images/ATFCAmap2.png">
	</div>
	<div class="courses__left">
		<img src="<?php echo get_template_directory_uri();?>/images/courses-left.png">
	</div>
	<div class="gredientoverlay" style="background: linear-gradient(270deg, #CB212121, #66212121, #212121);"></div>
</section>

<?php
$args = array(
	'post_type' => 'product',
	'orderby' => 'date',
	'order' => 'DESC',
	'post_status' => 'publish',
	'posts_per_page' => 3
);
$query = new WP_Query($args);
if ($query->have_posts()):
	global $product;
	$shopSmallTitle = get_field('home_shop_small_title');
	$shopTitle = get_field('home_shop_title');?>
	<section class="shop__online common__sect inline__block paddingtop">
		<div class="container">
			<div class="container__inner">
				<div class="common__info tacenter">
					<span class="tagline"><?php echo $shopSmallTitle;?></span>
					<h2><?php echo $shopTitle;?></h2>
				</div>
				<ul class="products columns-3">
					<?php
					while ( $query->have_posts() ) : $query->the_post();
						$shopIMG     = get_the_post_thumbnail_url();
						if(empty($shopIMG)){
							$shopIMG = get_template_directory_uri().'/images/placeholder.png';
						} ?>
						<li class="product type-product">
							<a href="<?php echo the_permalink(); ?>">
								<div class="home-shop-list" style="background-image: url(<?php echo $shopIMG;?>);"></div>
								<h2><?php the_title();?></h2>
								<span class="price"><?php echo $price = $product->get_price_html();?></span>
							</a> 
						</li>
					<?php endwhile; ?>
				</ul>
				<div class="shop_div inline__block tacenter">
					<a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) );?>" class="cmnbtn">shop all products <span class="gg-shape-triangle"></span></a>
				</div>
			</div>
		</div>
	</section>
<?php endif;?>

<?php wp_reset_query(); wp_reset_postdata();
$args = array(
	'post_type' => 'post',
	'orderby' => 'date',
	'order' => 'DESC',
	'post_status' => 'publish',
	'posts_per_page' => 3
);
$query = new WP_Query($args);
if ($query->have_posts()):
	/*$term_obj_list = get_the_terms( get_the_ID(), 'category' );
    $terms_string = join(', ', wp_list_pluck($term_obj_list, 'name'));*/
	$newsSmallTitle = get_field('home_news_small_title');
	$newsTitle 		= get_field('home_news_title');?>
	<section class="state__news common__sect inline__block paddingtop">
		<div class="container">
			<div class="container__inner">
				<div class="common__info tacenter">
					<span class="tagline"><?php echo $newsSmallTitle;?></span>
					<h2><?php echo $newsTitle;?></h2>
					<p><?php //echo $terms_string;?></p>
				</div>
				<div class="flex">
					<?php
					while ( $query->have_posts() ) : $query->the_post();
						$cat_list 	= get_the_terms( get_the_ID(), 'category' );
						$cat_string = "";
						foreach ( $cat_list as $term ) {
							if($term->name != "Uncategorized"){
								$cat_string .= $term->name."  ";
							}
						}
						$newsIMG    = get_the_post_thumbnail_url();
						if(empty($newsIMG)){
							$newsIMG = get_template_directory_uri().'/images/placeholder.png';
						} ?>
						<div class="overview-block-product">
							<a href="<?php echo the_permalink(); ?>">
								<div class="shopbg bgproperty" style="background-image: url('<?php echo $newsIMG;?>');"></div>
								<div class="state_details inline__block">
									<div class="category"><?php echo $cat_string;?></div>
									<h3><?php the_title();?></h3>
									<p><?php echo get_the_excerpt();?></p>
									<a class="cmnbtn" href="<?php echo the_permalink(); ?>">Read more <span class="gg-shape-triangle"></span></a>
								</div>
							</a>
						</div>
					<?php endwhile;?>
				</div>
			</div>
		</div>
	</section>
<?php endif;?>
<?php get_footer(); ?>
 