<?php
// General Woo stuff
add_theme_support( 'woocommerce' );


/**
 * Remove the breadcrumbs 
 */
add_action( 'init', 'woo_remove_wc_breadcrumbs' );
function woo_remove_wc_breadcrumbs() {
    remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
}

add_filter( 'body_class', 'tactical_gear_experts_woocommerce_active_body_class' );
function tactical_gear_experts_woocommerce_active_body_class( $classes ) {
    $classes[] = 'woocommerce-active';

    return $classes;
}

/**
 * Override theme default specification for product # per row
 */
add_filter('loop_shop_columns', 'loop_columns', 999);
function loop_columns() {
return 3; // 3 products per row
}

/*********Add AJAX when cart count update*********/
add_filter( 'woocommerce_add_to_cart_fragments', 'woo_cart_but_count' );
function woo_cart_but_count( $fragments ) {
    ob_start();
    $fragments['.in-cart'] = '<div class="in-cart">' . WC()->cart->get_cart_contents_count() . '</div>';    
    return $fragments;
}


add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args', 20 );
function jk_related_products_args( $args ) {
    $args['posts_per_page'] = 3; // 3 related products
    $args['columns'] = 3; // arranged in 3 columns
    return $args;
}