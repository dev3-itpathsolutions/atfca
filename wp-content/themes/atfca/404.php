<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package ATFCA
 */

get_header();
?>

<header class="slider__section inner__section inline__block">
	<div class="container tbl">
		<div class="tbl-cell">
			<div class="slider__content">
				<h2>404</h2>
			</div>
		</div>
	</div>
	<div class="bgoverlay" style="background-image: url('<?php echo get_template_directory_uri();?>/images/constitutions-banner.jpg');"></div>
	<div class="gredientoverlay" style="background: rgba(0, 0, 0, 0.396003);"></div>
</header>
<section class="default-page-sec">
	<div class="container">
		<h3>Error 404</h3>
		<p>This page could not be found!</p>
		<a href="<?php echo get_site_url(); ?>" class="cmnbtn"><span class="gg-shape-triangle"></span>Back to homepage</a>
	</div>
</section>

<?php
get_footer();
