<?php 
/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'atfca_enq_scripts' );
function atfca_enq_scripts() {
	
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '1');
	wp_enqueue_style( 'owl-carousel-css', get_template_directory_uri() . '/css/owl.carousel.min.css', array(), '1');
	wp_enqueue_style( 'custom', get_template_directory_uri() . '/css/custom.css', array(), '1');
    wp_enqueue_style( 'dev-custom-css', get_template_directory_uri() . '/css/dev-custom.css', array(), '1');
    wp_enqueue_style('css_overrides', get_template_directory_uri() . '/css/overrides.css', array(), '1.0.0', 'all' );

    wp_enqueue_script( 'jquery', get_stylesheet_directory_uri() . '/js/jquery.min.js', array('jQuery'), '1',true);
    wp_enqueue_script( 'owl-carousel-js', get_stylesheet_directory_uri() . '/js/owl.carousel.min.js', array(), '1',true);
    wp_enqueue_script( 'isotope-pkgd-js', get_stylesheet_directory_uri() . '/js/isotope.pkgd.min.js', array(), '1',true);
    wp_enqueue_script( 'scripts-js', get_stylesheet_directory_uri() . '/js/scripts.js', array(), '1',true);
	wp_enqueue_script( 'js_overrides', get_template_directory_uri() . '/js/overrides.js', array(), '1.0.0', true );
}