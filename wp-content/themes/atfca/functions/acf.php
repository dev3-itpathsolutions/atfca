<?php 

/**
*Acf Theme option 
*/
if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'Theme Options',
        'menu_title' => 'Theme Options',
        'menu_slug' => 'theme-option',
        'capability' => 'edit_posts',
        'redirect' => false,
    ));
}
/****** MAP KEY ************/
add_action('acf/init', 'my_acf_init');
function my_acf_init() {
    $MAPKEY = get_field('map_api_key', 'option');
    acf_update_setting('google_api_key', $MAPKEY);
}
?>