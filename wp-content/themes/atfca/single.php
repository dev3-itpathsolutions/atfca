<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package ATFCA
 */

get_header();
?>

<?php
while ( have_posts() ) : the_post();
	$term_obj_list = get_the_terms( get_the_ID(), 'category' );
	$terms_string  = join(' ', wp_list_pluck($term_obj_list, 'name'));
	$videTitle 	= get_field('post_video_title');
	$video 		= get_field('post_video');
	?>

	<section class="blog__single common__sect inline__block">
		<div class="container">
			<div class="container__inner single__inner">
				<div class="common__info tacenter">
					<span class="tagline"><?php echo $terms_string;?></span>
					<h2><?php echo get_the_title(); ?></h2>
					<div class="dates inline__block">
						<ul>
							<li><?php echo get_the_date('d M, Y'); ?></li>
							<li><?php the_author();?></li>
						</ul>
					</div>
				</div>
				<?php the_content();?>
				<?php if(!empty($video)){?>
					<div class="video-wrapper">
						<video loop="loop" muted="" playsinline="" class="has-media-controls-hidden">
							<source src="<?php echo $video;?>" type="video/mp4">
						</video>
							<div class="video-content">
								<h3><?php echo $videTitle ;?></h3>
							</div>
							<div class="gredientoverlay" style="background: rgba(0, 0, 0, 0.396003);"></div>            
					</div>
				<?php } ?>
			</div>
			<div class="social__share tacenter inline__block">
					<ul>
						<li>Share on:</li>
						<li><a target="_blank" href="http://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink();?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a target="_blank" href="http://twitter.com/intent/tweet/?text=<?php echo get_the_title();?>&url=<?php echo get_the_permalink();?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a target="_blank" href="https://plus.google.com/share?url=<?php echo get_the_permalink();?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
					</ul>
			</div>
		</div>
	</section>
	
	<?php 
    $related = atfca_get_related_blog( get_the_ID(), 3 );
	if( $related->have_posts() ):
	?>
	<section class="state__news common__sect inline__block paddingtop">
		<div class="container">
			<div class="container__inner">
				<div class="common__info tacenter">
					<span class="tagline">Discover</span>
					<h2>Similar news</h2>
				</div>
				<div class="flex">
					<?php 
				while( $related->have_posts() ): $related->the_post();
					$term_list = get_the_terms( get_the_ID(), 'category' );
					$cat_string  = join(' ', wp_list_pluck($term_list, 'name'));
					$re_postImage  = get_the_post_thumbnail_url();
					if(empty($re_postImage)){
						$re_postImage = get_template_directory_uri().'/images/placeholder.png';
					}?>
					<div class="overview-block-product">
						<a href="<?php echo get_permalink(); ?>">
							<div class="shopbg bgproperty" style="background-image: url('<?php echo $re_postImage;?>');"></div>
							<div class="state_details inline__block">
								<div class="category"><?php echo $cat_string;?></div>
								<h3><?php the_title();?></h3>
								<p><?php echo get_the_excerpt();?></p>
								<span class="cmnbtn">Read more <span class="gg-shape-triangle"></span></span>
							</div>
						</a>
					</div>
				<?php endwhile;?>
				</div>
			</div>
		</div>
	</section>
	<?php
	endif;
endwhile;	
get_footer();?>
