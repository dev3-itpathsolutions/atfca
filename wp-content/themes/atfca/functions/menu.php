<?php 


/**
*Register Menu function 
*/
add_action( 'init', 'register_my_menus' );
function register_my_menus() {
    register_nav_menus(
        array(
            'header-menu' => __( 'Header Menu' ),
            'footer-menu' => __( 'Footer Menu' )
        )
    );
}
/**
* Nev menu add class 
*/
add_action('nav_menu_css_class', 'add_current_nav_class', 10, 2 );
function add_current_nav_class($classes, $item)
{
    global $post;
    $current_post_type = get_post_type_object(get_post_type($post->ID));
    $current_post_type_slug = $current_post_type->rewrite['slug'];
    $menu_slug = strtolower(trim($item->url));
    if (strpos($menu_slug, $current_post_type_slug) !== false) {
        $classes[] = 'current-menu-item';
    }
    return $classes;
}
/*add_filter( 'nav_menu_link_attributes', 'wpse156165_menu_add_class', 10, 3 );
function wpse156165_menu_add_class( $atts, $item, $args ) {
    if( $args->theme_location == 'header-menu' ) {
        $class = 'navlink'; // or something based on $item
        $atts['class'] = $class;
    }
    return $atts;
}*/

?>