<?php 

/**
* Password Protected page logo url
*/
add_filter( 'login_headerurl', 'my_login_logo_url' );
function my_login_logo_url() {
    return home_url();
}

/**
*Wp-Login Page Design 
*/
add_action('login_head', 'custom_loginlogo');
function custom_loginlogo() {
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    $image = get_template_directory_uri().'/images/logo.svg';
    echo '<style type="text/css">
        .login h1 a {
            background-image: url('.$image.') !important;
            width: 100%;
            height: 186px;
            background-size:contain;
            background-position: center center;
            text-align: center;
        }
        body.login {
          background-color: #161717;
        }
    </style>';
}

/**
* Add HTML5 Support
*/
add_theme_support( 'html5',
         array(
          'comment-list',
          'comment-form',
          'search-form',
         )
);

//add_action( 'admin_menu', 'custom_menu_page_removing' );
function custom_menu_page_removing() {
	global $submenu;
	foreach($submenu['themes.php'] as $menu_index => $theme_menu){
        if($theme_menu[0] == 'Header' || $theme_menu[0] == 'Background' || $theme_menu[0] == 'Widgets')
        unset($submenu['themes.php'][$menu_index]);
    }
}

add_filter( 'excerpt_length', 'mytheme_custom_excerpt_length', 999 );
function mytheme_custom_excerpt_length( $length ) {
    return 20;
}

/*
* Load more functionaly of post ajax function 
*/
add_action('wp_ajax_nopriv_loadmorepost', 'loadmorepost');
add_action('wp_ajax_loadmorepost', 'loadmorepost');
function loadmorepost(){ 
  $gppp = (isset($_POST["ppp"])) ? $_POST["ppp"] : 9;
  $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;
  $postcat = (isset($_POST['postcat'])) ? $_POST['postcat'] : '';
  header("Content-Type: text/html");
  wp_reset_query();
  $responses = array();

  if($postcat == 'All') {
    $args = array(
      'post_type' => 'post',
      'post_status' => 'publish',
      'orderby' => 'date',
      'order' => 'DESC',
      'posts_per_page' => $gppp,
      'paged' => intval($page)
    );
  } else {
    $args = array(
      'post_type' => 'post',
      'post_status' => 'publish',
      'orderby' => 'date',
      'order' => 'DESC',
      'posts_per_page' => $gppp,
      'paged' => intval($page),
      'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field' => 'slug',
            'terms' => $postcat,
            'operator' => 'IN'
        ),
      )
    );
  }
  $query = new WP_Query($args);
  $postcount = $query->found_posts;
  $html = '';
  if ( $query->have_posts() ) :
    while ( $query->have_posts() ) : $query->the_post();
        $postID = get_the_ID();
        $sizes = "news-thumb";
        $postImage = get_the_post_thumbnail_url();
        if(empty($postImage)){
            $postImage = get_template_directory_uri().'/images/placeholder.png';
        }
        $permalink = get_the_permalink();
        $term_obj_list = get_the_terms( get_the_ID(), 'category' );
        $terms_string  = join(' ', wp_list_pluck($term_obj_list, 'name'));
        $title    = get_the_title();
        $excerpt  = get_the_excerpt();

      $html .=  '<div class="grid-item overview-block-product">
                    <a href="'.$permalink.'">
                        <div class="shopbg bgproperty" style="background-image: url('.$postImage.');"></div>
                        <div class="state_details inline__block">
                            <div class="category">'.$terms_string.'</div>
                            <h3>'.$title.'</h3>
                            <p>'.$excerpt.'</p>
                            <a href="'.$permalink.'" class="cmnbtn">Read more <span class="gg-shape-triangle"></span></a>
                        </div>
                </div>';
    endwhile;
  endif;
  wp_reset_postdata();
  $responses["count"] = $postcount;
  $responses["data"] = $html;
  echo json_encode($responses); 
  exit();
}

?>