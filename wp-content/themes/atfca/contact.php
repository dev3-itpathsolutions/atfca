<?php
/**
 * Template Name: Contact Template
 */
get_header(); ?>
<?php 
$introSMTitle = get_field('contact_small_title');
$introTitle   = get_field('contact_description');
$address  	  = get_field('contact_address');
$phone 	      = get_field('contact_phone_number');
$email 		  = get_field('contact_email_address');
$contactForm  = get_field('contact_form');
$location 	  = get_field('map');

$facebook 	= get_field('facebook', 'option');
$twitter 	= get_field('twitter', 'option');
$instagram 	= get_field('instagram', 'option');
?>
<section class="contact__sect common__sect inline__block">
	<div class="container">
		<div class="container__inner">
			<div class="flex">
				<div class="contact__left">
					<?php if(!empty($introSMTitle)){?>
						<span class="tagline"><?php echo $introSMTitle;?></span>
					<?php } ?>
						<h3><?php echo $introTitle;?></h3>
						<div class="contact__address">
							<?php if(!empty($address)){?>
								<div class="address__inner">
									<h4>Postal Address</h4>
									<p><?php echo $address;?></p>
								</div>
							<?php }

							if(!empty($phone) || !empty($email)) {?>
								<div class="address__inner">
									<h4>Contact Us</h4>
									<ul>
										<?php if(!empty($phone)){?>
											<li><a href="tel:<?php echo $phone;?>"><?php echo $phone;?></a></li>
										<?php } 
										if(!empty($email)){?>
											<li><a href="mailto:<?php echo $email;?>"><?php echo $email;?></a></li>
										<?php }?>
									</ul>
								</div>
							<?php } ?>
						</div>
						<?php if( !empty($location) ): ?>
	                        <div class="contact-map acf-map"  data-zoom="15">
	                            <div class="marker" data-lat="<?php echo esc_attr($location['lat']); ?>" data-lng="<?php echo esc_attr($location['lng']); ?>"></div>
	                        </div>
                    	<?php endif; ?>
						<?php if(!empty($facebook['url']) || !empty($twitter['url']) || !empty($instagram['url'])) {?>
							<div class="social__share inline__block">
								<ul>
									<li>Follow us on:</li>
									<?php if(!empty($facebook['url'])) { ?>
										<li><a href="<?php echo $facebook['url'];?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									<?php } 
									if(!empty($twitter['url'])) { ?>
										<li><a href="<?php echo $twitter['url'];?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
										<?php } 
									if(!empty($instagram['url'])) { ?>
										<li><a href="<?php echo $instagram['url'];?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
									<?php } ?>
								</ul>
							</div>
							<?php } ?>
					</div>
					<div class="contact__right">
						<?php echo $contactForm;?>
					</div>  
			</div>
		</div>
	</div>
</div>
</section>
<?php get_footer(); ?>
<?php $MAPKEY = get_field('map_api_key', 'option');?>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $MAPKEY;?>"></script>
<script type='text/javascript' src="<?php echo get_template_directory_uri(); ?>/js/google-map.js"></script>