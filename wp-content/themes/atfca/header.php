<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ATFCA
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.typekit.net/frc5lrn.css">
  	<link rel="stylesheet" href="https://use.typekit.net/frc5lrn.css">	
	<?php wp_head(); ?>
	<script>var BaseURL ="<?php echo get_template_directory_uri();?>"; </script>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
	
	<?php 
	$memberLogin = get_field('header_member_login','option');
	$joinNow     = get_field('header_join_now_link','option');
	$fixedLogo   = get_field('white_header_logo','option');
	if(empty($fixedLogo)){
    	$fixedLogo['url'] = get_template_directory_uri().'/images/fixed-logo.svg';
    }
    $custom_logo_id = get_theme_mod( 'custom_logo' );
	$logoImage = wp_get_attachment_image_src( $custom_logo_id , 'full' ); 
	if(empty($logoImage)){
		$logoImage[0] = get_template_directory_uri().'/images/logo.svg';
	}
	if(is_single() || is_page('contact')){
		$logoImage[0] = get_template_directory_uri().'/images/single-logo.svg';	
	}?>
	<nav class="header__section">
		<div class="top__section">
			<div class="top__inner">
				<ul>
					<li class="search__inner">
						<div class="search__form">
							<form role="search" method="get" id="search__form" action="<?php echo get_site_url();?>">
								<label class="screen-reader-text" for="s"></label>
								<input type="search" class="search__field" value="" name="s" id="s" placeholder="Search">
								<input type="submit" id="searchsubmit" value="Go">
							</form>
						</div>
					</li>
					<?php 
					if(!empty($memberLogin['url']) && !empty($memberLogin['title']) ) { ?>
						<li class="login"><a href="<?php echo $memberLogin['url']; ?>" <?php echo ($memberLogin['target'] ? 'target="_blank" rel="noreferrer" ' : ''); ?>><?php echo $memberLogin['title']; ?></a></li>						
					<?php } 

					if(!empty($joinNow['url']) && !empty($joinNow['title']) ) {	?>
						<li class="shopnow"><a href="<?php echo $joinNow['url']; ?>" <?php echo ($joinNow['target'] ? 'target="_blank" rel="noreferrer" ' : ''); ?>><?php echo $joinNow['title']; ?></a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<div class="container">
			<div class="header__inner desktop__version">
				<div class="header__logo">
					<a href="<?php echo get_site_url();?>">
						<img class="desktop__logo" src="<?php echo $logoImage[0];?>">
						<img class="fixed__logo" src="<?php echo $fixedLogo['url'];?>">
					</a>
				</div>
				<ul class="nav">
					<?php
					wp_nav_menu( 
						array(
							'menu'            => '',
							'theme_location'  => 'header-menu',
							'container'       => 'false',
							'menu_class'      => 'nav-item',
							'echo'            => true,
							'items_wrap'      => '%3$s'
						)
					); 

					global $woocommerce;
					$cart_page_url = function_exists( 'wc_get_cart_url' ) ? wc_get_cart_url() : $woocommerce->cart->get_cart_url();
					?>
					<li>
						<div class="cart-contents">             
							<a href="<?php echo $cart_page_url;?>" title="View your shopping cart">
								<img class="desktop-cart" src="<?php echo get_template_directory_uri();?>/images/cart.svg">
								<img class="fixed-cart" src="<?php echo get_template_directory_uri();?>/images/fixed-cart.svg">
								<div class="in-cart">0</div>
							</a>
						</div>
					</li>
				</ul>
			</div>
			<div class="header__inner mobile__version">
				<div class="header__mains">
					<div class="header__logo">
						<a href="<?php echo get_site_url();?>">
							<img src="<?php echo $logoImage[0];?>">
						</a>
					</div>
				</div>
				<div class="menu__section responsive__menu">
					<div class="cart-contents">             
						<a href="<?php echo $cart_page_url;?>" title="View your shopping cart">
							<img class="desktop-cart" src="<?php echo get_template_directory_uri();?>/images/cart.svg">
							<div class="in-cart">5</div>
						</a>
					</div>
					<div class="menu__toggle">
						<div class="one"></div>
						<div class="two"></div>
						<div class="three"></div>
					</div>
					<ul class="nav hidden">
						<?php
						wp_nav_menu( 
							array(
								'menu'            => '',
								'theme_location'  => 'header-menu',
								'container'       => 'false',
								'menu_class'      => 'nav-item',
								'echo'            => true,
								'items_wrap'      => '%3$s'
							)
						); ?>
					</ul>
					<div class="search__form">
						<form role="search" method="get" id="search__form" action="<?php echo get_site_url();?>">
							<label class="screen-reader-text" for="s"></label>
							<input type="search" class="search__field" value="" name="s" id="s" placeholder="Search">
							<input type="submit" id="searchsubmit" value="Go">
						</form>
					</div>
				</div>

				<div class="mobile__btns">
					<ul>
						<?php 
						if(!empty($memberLogin['url']) && !empty($memberLogin['title']) ) { ?>
							<li class="login"><a href="<?php echo $memberLogin['url']; ?>" <?php echo ($memberLogin['target'] ? 'target="_blank" rel="noreferrer" ' : ''); ?>><?php echo $memberLogin['title']; ?></a></li>						
						<?php } 

						if(!empty($joinNow['url']) && !empty($joinNow['title']) ) {	?>
							<li class="shopnow"><a href="<?php echo $joinNow['url']; ?>" <?php echo ($joinNow['target'] ? 'target="_blank" rel="noreferrer" ' : ''); ?>><?php echo $joinNow['title']; ?></a></li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
	</nav>
